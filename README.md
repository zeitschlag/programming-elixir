# Programming Elixir 1.6

- This is a public repo containing notes, codes, snippets, whatever for a private book club
- We read [Programming Elixir 1.6 by Dave Thomas][programming-elixir].
- We aim to meet once a week to discuss the progress.
- Life is more important, that's why we don't have a fixed day.
- We decide during a meeting about the next date and the next goal.

## Progress/Goals

### April 20th, 2023

- Chapter 1, Chapter 2 until April 26th, 2023.

### April 26th, 2023

- Chapter 2+ until May 1st, 2023

[programming-elixir]: https://pragprog.com/titles/elixir16/programming-elixir-1-6/