content = "Now is the time"

# open the file, store its content in `content`, close the file and search the
# content using a regex for User _lp. The underscore is required for macOS, as
# at least in my `/etc/passwd`, there was no user `lp`, but `_lp`.
lp =
  with {:ok, file} = File.open("/etc/passwd"),
       # note: same name as above
       content = IO.read(file, :all),
       :ok = File.close(file),
       # note: multiline-regex
       [_, uid, gid] = Regex.run(~r/^_lp:.*?:(\d+):(\d+)/m, content) do
    # note: content is content of file `etc/passwd` in this scope due to `with`
    "Group #{gid}, User #{uid}"
  end

IO.puts(lp)
IO.puts(content)
