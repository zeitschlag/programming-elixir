result =
  with {:ok, file} = File.open("/etc/passwd"),
       content = IO.read(file, :all),
       :ok = File.close(file),
       # the <- doesn't raise a MatchError, if it fails, 
       # but returns the value, that couldn't be matched.
       # in this case, nothing is matched, as there's no multiline-regex.
       # or user `xxx`
       [_, uid, gid] <- Regex.run(~r/^xxx:.*?:(\d+):(\d+)/, content) do
    "Group: #{gid}, User: #{uid}"
  end

IO.puts(inspect(result))
