people = [
  %{name: "Grumpy", height: 1.24}, 
  %{name: "Dave", height: 1.88}, 
  %{name: "Dopey", height: 1.32}, 
  %{name: "Shaquille", height: 2.16}, 
  %{name: "Sneezy", height: 1.28}, 
]

# the for-construct in this case is interesting and a bit more advanced:
# for person ... <- people iterates over people and assigns the current people to `person`.
# `height > 1.5` filters for people taller than 1.5 and works only in combination
# with `person = %{...}`, obviously.
# for returns a list.
IO.inspect(for person = %{height: height} <- people, height > 1.5, do: person)

