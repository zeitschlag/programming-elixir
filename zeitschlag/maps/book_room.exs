defmodule HotelRoom do

  def book( %{name: name, height: height} ) when height > 1.9 do
    IO.puts "Need extra-long bed for #{name}"
  end
  
  def book( %{name: name, height: height} ) when height < 1.3 do
    IO.puts "Need low shower controls for #{name}"
  end
  
  def book(person) do
    # person is required to have key `name`. It can be nil
    IO.puts "Need regular bed for #{person.name}"
  end
end
