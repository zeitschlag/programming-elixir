defmodule Attendee do
  defstruct name: "", paid: false, over_18: true
  
  def may_attend_after_party(attendee = %Attendee{}) do
    attendee.paid && attendee.over_18
  end
  
  # Book suggests this:
  # `print_vip_badge(%Attendee{})`
  # but it doesn't work as the default-value for `name` is `""`
  def print_vip_badge(%Attendee{name: ""}) do
    raise "missing name for badge"
  end

  def print_vip_badge(%Attendee{name: name}) do
    IO.puts "Very cheap badge for #{name}"
  end
end