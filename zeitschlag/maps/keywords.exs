defmodule Canvas do
  # this is one of those metadata-things, it's an @attribute
  @defaults [ fg: "black", bg: "white", font: "Merriweather" ]
  
  
  def draw_text(text, options \\ []) do
    # We use 
    options = Keyword.merge(@defaults, options)
    
    # we transform data into screen output
    IO.puts "Drawing text #{inspect(text)}"
    # 
    IO.puts "Foreground #{inspect(options[:fg])}"
    # 
    IO.puts "Background #{inspect(Keyword.get(options, :bg))}"
    IO.puts "Font #{inspect(Keyword.get(options, :font))}"
    IO.puts "Pattern #{inspect(Keyword.get(options, :pattern, "solid"))}"
    IO.puts "Style #{inspect(Keyword.get_values(options, :style))}"
  end
  
end
