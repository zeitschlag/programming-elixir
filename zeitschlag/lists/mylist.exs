defmodule MyList do
  def len([]), do: 0
  def len([_head|tail]), do: 1 + len (tail)
  
  def square(list) when is_list(list), do: map(list, &(&1*&1))
  def add_1(list) when is_list(list), do: map(list, &(&1+1))
  
  def map([], _fun), do: []
  def map([head|tail], fun), do: [fun.(head) | MyList.map(tail, fun)]
  
  # the initial_value feels like a hack, but we _need_ a value we can start with.
  def reduce([], initial_value, _fun), do: initial_value
  def reduce([head|tail], initial_value, fun) do
    # we need to provide the functions with all what each of them need, including
    # the recursion-anchor. that's why initial_value.
    reduce(tail, fun.(head, initial_value), fun)
  end
end