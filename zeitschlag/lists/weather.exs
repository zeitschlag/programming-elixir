# list of weather-station-data. Each entry is a four-element-list with this structure:
#
# [ timestamp, location_id, temperature, rainfall ]

defmodule WeatherHistory do
  defmodule Old do
    # To get all data for location 27, we run through all data and fetch only those
    # were location_id matches with 27.
    # no match for location 27
    def for_location27([]), do: []
    # second element (aka location_id) must be 27
    def for_location27([[time, 27, temp, rain] | tail]) do
      [[time, 27, temp, rain] | for_location27(tail)]
    end

    # also no match for location 27, continue with rest of list
    def for_location27([_ | tail]), do: for_location27(tail)
  end
  
  def for_location27(list), do: for_location(list, 27)

  def for_location([], _location), do: []
  # note, that we use pattern matching again to filter those location with match
  # location
  def for_location([head = [_, location, _, _] | tail], location) do
    [ head | for_location(tail, location)]
  end

  def for_location([_ | tail], location), do: for_location(tail, location)

  def test_data do
    [
      [1_366_225_622, 26, 15, 0.125],
      [1_366_225_622, 27, 15, 0.45],
      [1_366_225_622, 28, 21, 0.25],
      [1_366_229_222, 26, 19, 0.081],
      [1_366_229_222, 27, 17, 0.468],
      [1_366_229_222, 28, 15, 0.60],
      [1_366_232_822, 26, 22, 0.095],
      [1_366_232_822, 27, 21, 0.05],
      [1_366_232_822, 28, 24, 0.03],
      [1_366_236_422, 26, 17, 0.025]
    ]
  end
end
