defmodule Swapper do
  def swap([]), do: []
  def swap([a, b | tail]), do: [b, a | swap(tail)]
  def swap([c]), do: [c]
  # book suggests to raise an exception here, but we _can_ swap a single number???
end