# Nathan's Notes

Think of this an a notepad for generals remarks. I still take handwritten notes while reading the book.

- I already read the book two years ago but forgot almost everything.
- IEx is way more powerful than I remembered it. It feels like a pretty complete development environment with its `compile`/`recompile`/`pwd`/`ls`/... helpers

- [IEx][iex] also features autocomplete for modules
- Almost everything in Elixir seems to be command line, so it probably makes sense to get used to that. I've struggled with CLI and especially `vim` for years, but I still try to get a hang of it.
- Usually I prefer to have another editor like [BBEdit][bbedit] or [Textmate][textmate], but in that case I have to switch between windows all the time. There's an [Elixir plugin for IntelliJ][intellij-elixir], maybe this is worth a look? It might make my life easier.
  - I'll stick to BBEdit for writing Elixir-code for now, it has Autocomplete, Syntax Highlighting, I'm happy :)
  - Don't forget to set `ELIXIR_EDITOR` to `bbedit`, for example as [Universal Variable in Fish][so-universal-variable]
  - I'll also stick to my other beloved UI-tools like Fork for git etc.
  - Also to write more Elixir-code, I will follow the [Elixir-track on Exercism][exercism-elixir]. It differs from _the_ book, but I don't mind.

![](bbedit_elixir.png)

- Think of match-operation as equation Elixir tries to solve. If it can make the value of left side match the value of the right side (and may it be by binding values to variables), everything is fine.
- Variables on the right-hand side of a match operation are replaced by their values.
- if `mix test` fails on an Apple Silicon-Mac, try reinstalling `brew`, `elixir` and `erlang` after disabling Rosetta ([Source][brew-m1-elixir])
- The basic set of Elixir (types, concepts) seems to be quite small (integer, floating points, atoms, ranges, regexs, collections and immutability, pattern matching). This serves a foundation, and combined those concepts/types seem to be very powerful
- Some types seem to be pretty convenient, `DateTime` and `NaiveDateTime`, for example. I like that! Also: Lots of syntactic sugar, I guess? But the good kind?
- Operators are interesting, especially the truthy-stuff. `and` is strict, but `&&` is relaxed/truthy. Also `/` returns a float, while `div` returns an integer.
- I'd like to have a project to tinker around with elixir.

## Functions

- I still don't get this whole nested-functions-thing. Why should functions return functions?
> There are several reasons for returning functions. simplifying complex APIs and lazy evaluation are stuff that come to mind right now. One example would be Access.at/1 that returns a function to be used by get_in, pop_in, update_in, put_in functions. it's a good way to avoid certain functions to absorb more complexity than it needs. — [@cevado@hachyderm.io][why-return-functions]

- It's nice to know that it's _possible_, but the _why_ remains a mystery.
- Named functions _must_ be organized in modules.
- Using a guard-clause seems to be more elixir-y than type-checking in the function-body and raising an exception there.
- Elixir seems to be for clever, but lazy people and who doesn't wanna be clever and lazy?
- Programming (in Elixir) is about transforming data. And this is what functions do: They transform data. So: Think about how the transformation should look like. How should the result look like? And how do you get there?
- The definition of a list is recursive (head and tail with tail being a list), so recursion is a great tool to work with lists!

[intellij-elixir]: https://github.com/KronicDeth/intellij-elixir
[bbedit]: http://www.barebones.com/products/bbedit/index.html
[textmate]: https://macromates.com/
[brew-m1-elixir]: https://elixirforum.com/t/fresh-install-new-project-mix-test-causes-crash-1-14-3/54601/12
[iex]: https://hexdocs.pm/iex/IEx.html
[exercism-elixir]: https://exercism.org/tracks/elixir
[so-universal-variable]: https://stackoverflow.com/a/30187924
[why-return-functions]: https://hachyderm.io/@cevado/110333127494288415

- Elixir and its standard library are super powerful and very clever. Who would have thought, that accessors-functions not only take keys and key-list, but also functions?
- You can learn a lot by reading the documentation, knowledge is power is valid for Elixir, too!
- I try to be super fact when typing, but in the end I'm super hectic and due to that, I make lots of mistakes. That's super annoying when writing Elixir-Code. So maybe I should take more time? Build up speed over time, too?

## What do I want to talk about to flowfx?

- [ ] More IEx, maybe show how I develop?
- [ ] Lists and Recursion (often trial and error), Anchor-case
- [ ] Keyword _List_ (same key, ordered) vs. Map (Key unique) vs. Structs. Also: Modules.
- [ ] Structs are limited Maps. You can use (some) Map-module-functions (and pattern matching etc.)
- [ ] Modules and functions
  - [ ] guard
  - [ ] Pattern Matching (also: recursive), multiple bodies, default parameters, &
  - [ ] |>
  - [ ] Erlang-modules (atoms, help in `IEx`)