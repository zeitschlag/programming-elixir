# > Implement and run a function `sum(n)` that uses recursion
# > to calculate the sum of the integers from 1 to n. You'll need 
# > to write this function inside a module in a seperate file. 
# > Then load up IEx, compile that file and try your function

defmodule Sum do
  def until(0), do: 0
  def until(n), do: n + until(n-1)
end