# > Write a function `MyList.span(from, to)`, that returns a list of the numbers
# > from `from` up to `to`

defmodule MyList do
  # `Enum.to_list(from..to)` would be too easy.
  # How can I achive this using recursion or such things?
  def span(from, to) when from > to, do: []
  def span(from, to) do
      [from | span(from+1, to) ]
  end
end