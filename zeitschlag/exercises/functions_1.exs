# Go into IEx. Create and run the functions that do the following:

# > `list_concat.([:a, :b], [:c, :d]) # => [:a, :b, :c, :d]`
list_concat = fn list_a, list_b -> list_a ++ list_b end
result = list_concat.([:a, :b], [:c, :d])
IO.puts("list_concat.([:a, :b], [:c, :d]) => #{inspect(result)}")

# > `sum.(1,2,3) # => 6`
sum = fn a, b, c -> a + b + c end
result = sum.(1, 2, 3)
IO.puts("sum.(1, 2, 3) => #{inspect(result)}")

# > `pair_tuple_to_list.({ 1234, 5678 }) # => [1234, 5678]
pair_tuple_to_list = fn {a, b} -> [a, b] end
result = pair_tuple_to_list.({1234, 5678})
IO.puts("pair_tuple_to_list.({1234, 5678}) => #{inspect(result)}")
