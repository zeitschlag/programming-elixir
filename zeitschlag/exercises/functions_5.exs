# > Use the `&` notation to rewrite the following
# > `Enum.map [1,2,3,4], fn x -> x + 2`

plus_two = Enum.map [1, 2, 3, 4], &(&1 + 2)
IO.puts(inspect(plus_two))

# > `Enum.each [1,2,3,4], fn x -> IO.inspect x end`

Enum.each [1,2,3,4], &IO.inspect/1