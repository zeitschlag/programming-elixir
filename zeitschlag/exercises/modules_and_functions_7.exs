# > Find the library functions to do the following, and then use each in IEx
# > (If the word _Elixir_ or _Erlang_ appears at the end of the challenge,
# > then you'll find the answer in that set of libraries.

# > 1. Convert a float to a string with two decimal digits (Erlang)

IO.puts inspect(:erlang.float_to_binary(5.123123, [{:decimals, 2}]))

# > 2. Get the value of an operating-system environment variable (Elixir)

IO.puts inspect(System.get_env("ELIXIR_EDITOR"))

# > 3. Return the extension component of a file name (elixir)

IO.puts inspect(Path.extname("modules_and_functions7.exs"))

# > 4. Return the process's current working directory (Elixir)

IO.puts inspect(File.cwd)

# > 5. Convert a string containing JSON into Elixir data structures (just find; don't install)
# Answer: module: json then: 
# `JSON.decode(string_containing_json)`
# HELLO SWIFT IS THAT YOU!?

# > 6. Execute a command in your operating system's shell

IO.puts inspect(System.cmd("pwd", [])) #