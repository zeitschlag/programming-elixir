# Exercise: ModulesAndFunctions-6
# > I'm thinking of a number between 1 and 1000...

defmodule Chop do
  # guess halfway  
  def guess(actual, lower..upper) when div(lower+upper, 2) == actual do
    guess = div(lower+upper, 2)
    IO.puts "Is it #{guess}"
    IO.puts "#{guess}"
    actual
  end
  
  # if guess is too small, the answer lies between one more than our guess and the end
  def guess(actual, lower..upper) when actual > div(lower+upper, 2) do
    guess = div(lower+upper, 2)
    IO.puts "Is it #{guess}"
    guess(actual, guess+1..upper)
  end

  # if guess is too big, the answer lies between the bottom and one less than our guess
  def guess(actual, lower..upper) when actual < div(lower+upper, 2) do
    guess = div(lower+upper, 2)
    IO.puts "Is it #{guess}"
    guess(actual, lower..guess-1)
  end  
end

Chop.guess(273, 1..1000)