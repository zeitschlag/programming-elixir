# > Write a function prefix that takes a string. It should return a new function
# > that takes a second string. When the second function is called, it will return
# > a string containing the first string, a space, and the second string

prefix = fn first_param -> 
  fn second_param -> 
    "#{first_param} #{second_param}"
  end
end

mrs = prefix.("Mrs")
IO.puts inspect(mrs.("Smith"))
IO.puts inspect(prefix.("Elixir").("Rocks"))