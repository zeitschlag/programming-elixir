defmodule MyList do
  # > Exercise: ListsAndRecursion-1
  # > Write a `mapsum` function that takes a list and a function.
  # > It applies the function to each element of the list and then
  # > sums the result.
  def mapsum([], _func), do: 0
  def mapsum([head|tail], func), do: func.(head) + mapsum(tail, func)

  # > Exercise: ListsAndRecursion-2
  # > Write a `max(list)` that returns the element with 
  # > the maximum value in the list.
  # > (This is slightly trickier than it sounds)

  # this is the anchor: if the list has only one element, this is the maximum.
  def max([head]), do: head
  # the maximum element of a list is either the head or the maximum element of the rest
  def max([head|tail]), do: MyList.max(head, max(tail))
  
  # these are three helper-functions that compare two values and return the larger
  # the possible solution from the book uses Kernel.max/2 instead of these three helpers.
  def max(a, a), do: a  
  def max(a, b) when a > b, do: a
  def max(a, b) when a < b, do: b
 
  # > An Elixir single-quoted string is actually a list of individual character codes
  # > Write a `caesar(list, n)` function that adds `n` to each list element
  # > wrapping if the addition results in a character greater than z
  def caesar([], _n), do: []
  def caesar([head|tail], n) do
    [caesar_char(head, n) | caesar(tail, n)]
  end
  
  # with wrapping (aka - 26) when necessary.
  defp caesar_char(char, n) when n + char > ?z, do: n + char - 26
  # without wrapping
  defp caesar_char(char, n), do: char + n
end