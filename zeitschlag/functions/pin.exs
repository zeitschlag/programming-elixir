defmodule Greeter do
  def for(name, greeting) do
    fn
      # this translates: 
      # we bind `name` to a value in `for/2`.
      # And in this inner function we check if the function
      # is invoked with a parameter we already know.
      (^name) -> "#{greeting} #{name}"
      (name) -> "I don't know you, *checks notes* #{name}"
    end
  end
end

mr_valim = Greeter.for("José", "Oi!")

IO.puts mr_valim.("José")
IO.puts mr_valim.("Dave")