# This causes a warning as 
defmodule DefaultParams1 do
  def func(p1, p2 \\ 123) do
    IO.inspect [p1, p2]
  end
  
  def func(p1, 99) do
    IO.puts "You said 99"
  end
end

defmodule DefaultParams2 do

  # to silence the warning from DefaultParams1,
  # we must add a function head with the default
  # parameters. This should reduce confusing when
  # using default parameters.
  def func(p1, p2 \\ 123)
  
  # but for some reason, the rest of DefaultParams2
  # differs from DefaultParams1 in the book.
  # This is the equivalent from DefaultParams1:
  def func(p1, 99) do
    # If p2 is 99, we say it's 99
    IO.puts "You said 99"
  end
  
  def func(p1, p2) do
    # If it's not, we print a string (?????)
    IO.inspect [p1, p2]
  end
end