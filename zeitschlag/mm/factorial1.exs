defmodule Factorial do
  def of(0), do: 1
  def of(n) when is_integer(n) and n > 0 do
   n * of(n-1)
   end
end

defmodule Sum do
  # The order of clauses is important!
  # When searching for the implementation to run,
  # Elixir goes from top to bottom, executing the first match
  def of_n_numbers(0), do: 0
  def of_n_numbers(n), do: n + of_n_numbers(n-1)
  # the other way round wouldn't work.
  # Also: Elixir wouldn't allow compiling this!
end

defmodule ListLength do
  def len([]), do: 0
  def len([_head|tail]), do: 1 + len(tail)
end