defmodule Outer do
  # Doesn't have to be nested, but can also be defined as `defmodule Outer.Inner`
  defmodule Inner do
    def func do
      IO.puts "Inner!"
    end
  end
end

defmodule Mod do
  def func1 do
    IO.puts "in func1"
  end
  
  def func2 do
    func1
    IO.puts "in func2"
  end
end
